# README #
This an example project showing the usage of
```swift
class BarcodeGenerator
```
```swift
class BarcodeReader
```
```swift
class BarcodeReaderViewController
```

# Summary #
### Target ###
Encoding / Decoding Barcodes with only native iOS frameworks

Supported Barcodes Types:
* QR
* Aztec
* PDF417
* Code128

### Decoding ###
* from camera - `AVFoundation` iOS 7.0+
* from image - `CIDetector` iOS 5.0+
### Encoding ###
* `CIFilter` iOS 5.0+

## Code structure ##

```swift
class BarcodeGenerator {
    // returns `CIImage` of generated barcode or `nil` in case of error
    static func barcode(from string : String, type : CICategoryGenerator, encoding = String.Encoding.utf8) -> CIImage?`
    
    // returns `UIImage` of specified size or `nil` if given `ciImage` is `nil`
    static func getUIImage(from ciImage: CIImage?, size: CGSize) -> UIImage?`
}
```

```swift
class BarcodeReader {
    // returns array of `String`s decoded from QR codes found in given `uiImage`
    // returns empty array in case "no QR codes decoded"
    static func read(from uiImage: UIImage!) -> [String]
}
```

```swift
class BarcodeReaderViewController {
    // capture session
    var captureSession = AVCaptureSession()
    // frame showing a detected barcode
    var qrCodeFrameView: UIView?
    // handler for array of decoded barcodes
    var onBarcodesRead: (([AVMetadataMachineReadableCodeObject]) -> Void)?
    // ignore all detected barcodes in a frame except one
    var returnOnlyFirstBarcode: Bool = false
    // what barcode types should be detected
    var metadataObjectTypes : [String] = [AVMetadataObjectTypeQRCode,
                                          AVMetadataObjectTypeAztecCode,
                                          AVMetadataObjectTypePDF417Code,
                                          AVMetadataObjectTypeCode128Code]
    // `metadataObjectTypes` should be set before calling this function
    func initializeAndStartBarcodeScanning() { ... }
}
```

## Usage example ##

### Encoding ###
```swift
let ciImage = BarcodeGenerator.barcode(from: contentTextField.text!, type: .CIQRCodeGenerator)
let uiImage = BarcodeGenerator.getUIImage(from: ciImage, size: imageView.frame.size)
```

### Decoding from image ###
```swift
let barcodes : [String] = BarcodeReader.read(from: image)
```
### Decoding from camera ###
```swift
class ScannerViewController: BarcodeReaderViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.returnOnlyFirstBarcode = true
        
        self.onBarcodesRead = { barcodes in
            self.captureSession.stopRunning()
            
            var msg = ""
            for barcode in barcodes {
                msg += "type=\(barcode.type) value=\"\(barcode.stringValue)\"\n\n"
            }
            
            let alert = UIAlertController(title: "\(barcodes.count) barcodes found", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                self.captureSession.startRunning()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        /*
         self.metadataObjectTypes = [AVMetadataObjectTypeQRCode,
                                     AVMetadataObjectTypeAztecCode,
                                     AVMetadataObjectTypePDF417Code]
         */
        
        initializeAndStartBarcodeScanning()
    }
}
```
