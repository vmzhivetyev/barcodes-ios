//
//  BarcodeGenerator.swift
//  qr-and-more
//
//  Created by Вячеслав Живетьев on 15.07.17.
//  Copyright © 2017 Base team. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

open class BarcodeGenerator {
    
    public enum CICategoryGenerator : String {
        case CIAztecCodeGenerator
        case CICode128BarcodeGenerator
        case CIPDF417BarcodeGenerator
        case CIQRCodeGenerator
    }
    
    public static func barcode(from string: String, type: CICategoryGenerator, encoding: String.Encoding = String.Encoding.utf8) -> CIImage? {
        if string == "" {
            return nil
        }
        
        let data = string.data(using: encoding, allowLossyConversion: false)
        
        // docs 
        // https://developer.apple.com/library/content/documentation/GraphicsImaging/Reference/CoreImageFilterReference/#//apple_ref/doc/uid/TP30000136-SW142
        let filter = CIFilter(name: type.rawValue)!
        filter.setValue(data, forKey: "inputMessage")
        //filter.setValue("H", forKey: "inputCorrectionLevel") // error-correction level: H=30%, Q=25%, M=15% (default), L=7%
        
        return filter.outputImage
    }
    
    public static func getUIImage(from ciImage: CIImage?, size: CGSize) -> UIImage? {
        if ciImage != nil {
            let scaleX = size.width / (ciImage!.extent.size.width)
            let scaleY = scaleX//frame.size.height / (ciImage.extent.size.height)!
            
            let transformedImage = ciImage!.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
            
            return UIImage(ciImage: transformedImage)
        }
        
        return nil
    }
}
