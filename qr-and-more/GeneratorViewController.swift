//
//  GeneratorViewController.swift
//  qr-and-more
//
//  Created by Вячеслав Живетьев on 14.07.17.
//  Copyright © 2017 Base team. All rights reserved.
//

import UIKit

class GeneratorViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var barcodeTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    let segmentsValues : [BarcodeGenerator.CICategoryGenerator] = [.CIQRCodeGenerator,
                                                                .CIAztecCodeGenerator,
                                                                .CIPDF417BarcodeGenerator,
                                                                .CICode128BarcodeGenerator]
    
    @IBAction func textFieldValueChanged(_ sender: Any) {
        let targetType = segmentsValues[barcodeTypeSegmentedControl.selectedSegmentIndex]
        let imgBuf = BarcodeGenerator.barcode(from: contentTextField.text!, type: targetType)
        
        imageView.image = BarcodeGenerator.getUIImage(from: imgBuf, size: imageView.frame.size)
    }
    
    @IBAction func segmentChanged(_ sender: Any) {
        textFieldValueChanged(AnyObject.self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == contentTextField {
            contentTextField.resignFirstResponder()
        }
        
        return true
    }
}
