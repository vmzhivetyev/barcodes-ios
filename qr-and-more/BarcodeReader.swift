//
//  BarcodeReader.swift
//  qr-and-more
//
//  Created by Вячеслав Живетьев on 15.07.17.
//  Copyright © 2017 Base team. All rights reserved.
//

import Foundation
import UIKit

class BarcodeReader {
    
    static func read(from uiImage: UIImage!) -> [String]! {
        let detector:CIDetector=CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])!
        let ciImage:CIImage=CIImage(image: uiImage)!
        
        var arr : [String] = []
        
        let features=detector.features(in: ciImage)
        for feature in features as! [CIQRCodeFeature] {
            if feature.messageString != nil {
                arr.append(feature.messageString!)
            }
        }
        
        return arr
    }
}
