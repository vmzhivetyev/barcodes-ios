//
//  ScannerViewController.swift
//  qr-and-more
//
//  Created by Вячеслав Живетьев on 17.07.17.
//  Copyright © 2017 Base team. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: BarcodeReaderViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var imagePicker = UIImagePickerController()
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            picker.dismiss(animated: true, completion: {
                
                let barcodes = BarcodeReader.read(from: image)
                var msg = ""
                for barcode in barcodes! {
                    msg += "value=\"\(barcode)\"\n\n"
                }
                
                let alert = UIAlertController(title: "\(barcodes!.count) qr codes found", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    self.captureSession.startRunning()
                }))
                self.present(alert, animated: true, completion: nil)
            });
        }
        
    }
    
    @IBAction func cameraRollBtnClicked(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            self.captureSession.stopRunning()
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.returnOnlyFirstBarcode = true
        
        self.onBarcodesRead = { barcodes in
            self.captureSession.stopRunning()
            
            var msg = ""
            for barcode in barcodes {
                msg += "type=\(barcode.type) value=\"\(barcode.stringValue)\"\n\n"
            }
            
            let alert = UIAlertController(title: "\(barcodes.count) barcodes found", message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                self.captureSession.startRunning()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        /*
         self.metadataObjectTypes = [AVMetadataObjectTypeQRCode,
                                     AVMetadataObjectTypeAztecCode,
                                     AVMetadataObjectTypePDF417Code]
         */
        
        initializeAndStartBarcodeScanning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.captureSession.stopRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.captureSession.startRunning()
    }
}
