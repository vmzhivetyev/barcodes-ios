//
//  BarcodeReaderViewController.swift
//  qr-and-more
//
//  Created by Вячеслав Живетьев on 13.07.17.
//  Copyright © 2017 Base team. All rights reserved.
//

import UIKit
import AVFoundation

class BarcodeReaderViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var qrCodeFrameView: UIView?
    
    var onBarcodesRead: (([AVMetadataMachineReadableCodeObject]) -> Void)?
    var returnOnlyFirstBarcode: Bool = false

    var metadataObjectTypes : [String] = [AVMetadataObjectTypeQRCode,
                                               AVMetadataObjectTypeAztecCode,
                                               AVMetadataObjectTypePDF417Code,
                                               AVMetadataObjectTypeCode128Code]
    
    func initializeAndStartBarcodeScanning() {
        self.view.backgroundColor = UIColor.clear
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let input: AnyObject!
        
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
        }
        catch {
            return
        }
        
        captureSession = AVCaptureSession()
        captureSession.addInput(input as! AVCaptureInput)
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(captureMetadataOutput)
        
        let queue = DispatchQueue(label: "com.barcodes.metadata", attributes: DispatchQueue.Attributes.concurrent)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: queue)
        captureMetadataOutput.metadataObjectTypes = metadataObjectTypes
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.insertSublayer(videoPreviewLayer!, at: 0)
        
        captureSession.startRunning()
        
        qrCodeFrameView = UIView()
        qrCodeFrameView?.layer.borderColor = UIColor.green.cgColor
        qrCodeFrameView?.layer.borderWidth = 2
        view.addSubview(qrCodeFrameView!)
        view.bringSubview(toFront: qrCodeFrameView!)
    }
    
    open func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        if self.onBarcodesRead == nil {
            return
        }
        
        var arr: [AVMetadataMachineReadableCodeObject] = []
        
        for metadataObject in metadataObjects {
            if let videoPreviewLayer = self.videoPreviewLayer {
                if let transformedMetadataObject = videoPreviewLayer.transformedMetadataObject(for: metadataObject as! AVMetadataObject) {
                    if transformedMetadataObject.isKind(of: AVMetadataMachineReadableCodeObject.self) {
                        let barcodeObject = transformedMetadataObject as! AVMetadataMachineReadableCodeObject
                        // not the best solution
                        drawRect(rect: barcodeObject.bounds)
                        arr.append(barcodeObject)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.drawRect(rect: CGRect.zero)
                        })
                        
                        if returnOnlyFirstBarcode {
                            onBarcodesRead!(arr)
                            return
                        }
                    }
                }
            }
        }
        
        if arr.count > 0 {
            onBarcodesRead!(arr)
        }
    }
    
    func drawRect(rect: CGRect!) {
        DispatchQueue.main.async {
            self.qrCodeFrameView?.frame = rect
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
